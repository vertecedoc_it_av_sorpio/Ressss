  
--PD
x1 :: Float
x1 = do {
         print ("cual es x1?")
         x1 <- getLine
         }

y1 :: Float
y1 = do {                                                                               
         print ("cual es y1?")                                                          
         y1 <- getLine                                                                  
         }   

--PA
x2 :: Float
x2 = do {                                                                               
         print ("cual es x2?")                                                          
         x2 <- getLine                                                                  
         }   

y2 :: Float
y2 = do {                                                                               
         print ("cual es y2?")                                                          
         y2 <- getLine                                                                  
         }   
--------------------------------------------------------
--PC
x3 :: Float
x3 = do {                                                                               
         print ("cual es x3?")                                                          
         x3 <- getLine                                                                  
         }   

y3 :: Float
y3 = do {                                                                               
         print ("cual es y3?")                                                          
         y3 <- getLine                                                                  
         }   

--Usar con PD
--PB
x4 :: Float
x4 = do {                                                                               
         print ("cual es x4?")                                                          
         x4 <- getLine                                                                  
         }   

y4 :: Float
y4 = do {                                                                               
         print ("cual es y4?")                                                          
         y4 <- getLine                                                                  
         }   

--listas de pendientes para el problema
--PD PA
--pdpa = [ypd, ypa, xpd, xpa]
--PD PC
--pdpc = [ypd, ypc, xpd, xpc]

-- probar con Ypd Ypa Xpd Xpa
 
pendiente y1 y2 x1 x2 = ((y1 - y2)/(x1 -x2))



f :: Show a => a -> String
f x = show x

m1 :: Float                                                                       
m1 = pendiente y1 y2 x1 x2
m1' = f m1

m2 :: Float                                                                 
m2 = pendiente y1 y2 x1 x2  
m2' = f m2

m3 :: Float
m3 = pendiente y1 y2 x1 x2
m3' = f m3  

m4 :: Float
m4 = pendiente y1 y2 x1 x2
m4' = f m4

ang ma mb = (((atan ((ma - mb)/(1 + ma - mb))) * 180) / pi)

ang1 = ang m1 m2
ang1' = f ang1

ang2 = ang m3 m4
ang2' = f ang2

main = do

  print ("PdPa " ++ m1' ++ " , " ++ "PdPc " ++ m2' ++ " , " ++ "PcPb " ++ m3' ++ " , " ++ "PaPb " ++ m4')

  --putStrLn ("el angulo entre las rectas PdPa y PdPc es: " ++ ang1')
  --putStrLn ("el angulo entre las rectas PcPb y PaPb es: " ++ ang2')
