/*
 Formulas
μ = m/l					solo para ondas en cuerdas
V = sqrt(F/μ) || sqrt(Fl/m)

____________________________

V = λ/T || λf
f = V/λ || 1/(λ/V)
T = λ/V || 1/(V/λ)

____________________________
Datos:
Fuerza = 80N
Longuitud = 50cm
Masa = 500gr

##### Problema 2 ########
Ulambre metalico de 500gr y 50cm de longitudestabajo una tension 
de 80 newtons
¿cual es la velocidad transversal de alambre?
*/
#include <iostream>
#include <cmath>

using namespace std;

int main() {
	double E;
	double m = 0.3;
	double F = 48;
	double l = 2; /* pasado a mts */
	double μ = m/l;
	double f = 2;
	double A = 0.05;
	cout<<"Usando la formula E=2pi2 f2 μ2 A2 l\n";
	cout.precision(80);
	E = 2*pow(3.1415,2)*pow(f,2)*pow(μ,2)*pow(A,2)*l;
	cout<<E<<" WATS\n";
		/*
	 sqrt(80N/(0.5/0.5))
	 sqrt(80N/(1))
	 sqrt(80) = 8.94427191
	 */

return 0;
}
