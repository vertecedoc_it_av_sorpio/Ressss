una cuerda de 1.2kg se estira 5.2 metros con una tension 120N
problema obten:
la velocidad

## sqrt() significa raiz cuadrada ##

Formulas
μ = m/l					solo para ondas en cuerdas
V = sqrt(F/μ) || sqrt(Fl/m)

____________________________

V = λ/T || λf
f = V/λ || 1/(λ/V)
T = λ/V || 1/(V/λ)

____________________________

Datos
Masa = 1.2kg
Longitud = 5.2m
Fuerza = 120N

Resultado

V = sqrt(Fl/m)
V = sqrt(120N * 5.2m / 1.2kg)
v = sqrt(120[Kg/seg2] * 5.2[m] / 1.2[kg])
v = sqrt(624[Kg*m2/seg2] / 1.2[kg])
v = sqrt(520[m2/seg2])
v = 22.803508502 [m/seg]
