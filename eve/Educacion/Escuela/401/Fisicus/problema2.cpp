/*
 Formulas
μ = m/l					solo para ondas en cuerdas
V = sqrt(F/μ) || sqrt(Fl/m)

____________________________

V = λ/T || λf
f = V/λ || 1/(λ/V)
T = λ/V || 1/(V/λ)

____________________________
Datos:
Fuerza = 80N
Longuitud = 50cm
Masa = 500gr

##### Problema 2 ########
Ulambre metalico de 500gr y 50cm de longitudestabajo una tension 
de 80 newtons
¿cual es la velocidad transversal de alambre?
*/
#include <iostream>
#include <cmath>

using namespace std;

int main() {
	double v;
	double m = 0.5;
	double F = 80;
	double l = 0.5; /* pasado a mts */
	double μ = m/l;

	cout<<"Usando la formula sqrt(F/μ)\n";
	cout.precision(80);
	v = sqrt(F/μ);
	cout<<v<<"m/s\n";
		/*
	 sqrt(80N/(0.5/0.5))
	 sqrt(80N/(1))
	 sqrt(80) = 8.94427191
	 */


	cout<<"\nusando formula sqrt(Fl/m)\n";
	v = sqrt(F*l/m);
	/*
	 sqrt(80N*0.5m/0.5kg)
	 sqrt(40/0.5)
	 sqrt(80)
	 = 8.94427191
	 */
	cout.precision(80);
	cout<<v<<"m/s";
	return 0;
}
