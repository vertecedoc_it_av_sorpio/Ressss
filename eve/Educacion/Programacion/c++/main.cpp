/* El codigo original en rust
	The original code in rust
    
#![feature(llvm_asm)]

#![no_std]
#![no_main]

use ruduino::cores::atmega328::port;
use ruduino::delay::delay_ms as del;
use ruduino::Pin;
#[no_mangle]
fn main() {
    let dol: u32 = (400/2);
    port::B0::set_output();
    port::B1::set_output();
    port::B2::set_output();
    port::B3::set_output();
    port::B4::set_output();
    port::B5::set_output();
    port::D1::set_output();
    port::D2::set_output();
    port::D3::set_output();
    port::D4::set_output();
    port::D5::set_output();
    port::D6::set_output();
    port::D7::set_output();

    loop {
        port::D1::set_high();
        del(&dol);
        port::D2::set_high();
        del(&dol);
        port::D1::set_low();

        port::D3::set_high();
        del(&dol);
        port::D2::set_low();
        port::D4::set_high();
        del(&dol);
        port::D3::set_low();

        port::D5::set_high();
        del(&dol);
        port::D4::set_low();
        port::D6::set_high();
        del(&dol);
        port::D5::set_low();

        port::D7::set_high();
        del(&dol);
        port::D6::set_low();
        port::B0::set_high();
        del(&dol);
        port::D7::set_low();

        port::B1::set_high();
        del(&dol);
        port::B0::set_low();
        port::B2::set_high();
        del(&dol);
        port::B1::set_low();

        port::B3::set_high();
        del(&dol);
        port::B2::set_low();
        port::B4::set_high();
        del(&dol);
        port::B3::set_low();

        port::B5::set_high();
        del(&dol);
        port::B4::set_low();
        del(&dol);
        port::B5::set_low();
    }
}


*/
int dol = 400/2;
void setup()
{
  
  pinMode(12, OUTPUT);  
  pinMode(11, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(2, OUTPUT);
  pinMode(1, OUTPUT);
}

void loop()
{
  digitalWrite(13, LOW); 
  digitalWrite(1, HIGH);
  delay(dol);
  digitalWrite(2, HIGH);
  delay(dol);
  digitalWrite(1, LOW);
  
  digitalWrite(3, HIGH);
  delay(dol);
  digitalWrite(2, LOW);
  digitalWrite(4, HIGH);
  delay(dol);
  digitalWrite(3, LOW);
  
  digitalWrite(5, HIGH);
  delay(dol);
  digitalWrite(4, LOW);
  digitalWrite(6, HIGH);
  delay(dol);
  digitalWrite(5, LOW);
  
  digitalWrite(7, HIGH);
  delay(dol);
  digitalWrite(6, LOW);
  digitalWrite(8, HIGH);
  delay(dol);
  digitalWrite(7, LOW);
  
  digitalWrite(9, HIGH);
  delay(dol);
  digitalWrite(8, LOW);
  digitalWrite(10, HIGH);
  delay(dol);
  digitalWrite(9, LOW);
  
  digitalWrite(11, HIGH);
  delay(dol);
  digitalWrite(10, LOW);
  digitalWrite(12, HIGH);
  delay(dol);
  digitalWrite(11, LOW);
  
  digitalWrite(13, HIGH);
  delay(dol);
  digitalWrite(12, LOW);
  delay(dol);
}