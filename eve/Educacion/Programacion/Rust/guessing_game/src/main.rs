use std::io;
use rand::Rng;
use std::cmp::Ordering;

fn main(){
    println!("Adivina el numero");
    let mut guess = String::new();
    let mut secret_number = rand::thread_rng().gen_range(1,90001);
    loop {
        println!("{ob}el numero es: {}", secret_number, ob = "\n");
        println!("Escribe la adivinanza");
        guess = String::new();
        io::stdin()
            .read_line(&mut guess)
            .expect("Error al leer la linea");

        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue
        };
        println!("Adivinaste: {}", guess);

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Muy pequeño!"),
            Ordering::Greater => println!("Muy grande!"),
            Ordering::Equal => {
                println!("Ganaste!");
                break;
            }
        }

        secret_number = rand::thread_rng().gen_range(1,90001);
    }
}
