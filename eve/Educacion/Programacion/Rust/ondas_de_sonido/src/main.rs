use std::*;
use std::time::Duration;
use std::thread::sleep as sl;
fn main() {
    println!("\n\tCODGIGO CREADO POR CARLOS EDUARDO PALMA LICEA, 
    NO UTILIZABLE PARA ENTREGARLO COMO TRABAJO PROPIO ALMENOS QUE SEAS YO XD
    
    ESTE CODIGO ES UN CODIGO DE BAJA CALIDAD DADA LAS CARENCIAS QUE POSEE,
    LA FALTA DE FUNCIONES PERZONALIZADAS Y EL USO CASI ABSOLUTO DE TIPO FLOTANTE 
    DE 32 BITS Y EL USO DE PROGRAMACION IMPERATIVA, RECOMIENDO UTILIZARLO UNICAMENTE PARA ANALIZAR LOS PRINCIPIOS BASICOS\n\n");
    //primer problema
    println!("\n\n\n//primer problema\n");
    print!("Calcula la velocidad del sonido en una varilla de cobre que tiene una densidad de 8,800 kg/m^3 \nEl módulo de Young para el sonido es de 11 x 10^10 Pa. Solución=3540 m/s");
    println!("\nusando la formula: √(γ/ρ)\n");
    let gamma: f32 = 11.0 * 10f32.powf(10.0);
    let ro: f32 = 8800.0;
    let ob: f32 = gamma/ro;
    let r1: f32 = ob.sqrt();

    println!("resultado es {} m/s\n\n",r1);
    sl(Duration::from_secs(20));

    // segundo problema 
    println!("\n\n\n//segundo problema\n");
    println!("Compare las velocidades teóricas del sonido en hidrógeno (γ =1.4) y en helio (γ=1.66) a 0°C.\nLas masas moleculares para el hidrógeno y para el helio son M H =2.0 y M He = 4.0 Solución = V He = 0.77v H\n");
    println!("existen dos formulas:\n   γP    y   γRT   dado que no tenemos P(resion) usaremos la segunda formula\n√(-----)   √(----)\n    ρ         M\n");
                                     
    let gamma_h = 1.4;
    let gamma_he = 1.66;
    let mut T = 0f32 /* Centigrados */;
    let masa_molar_h = 2f32;
    let masa_molar_he = 4f32;
    let R = 8.3135;
    let r2_h = ((gamma_h * R * (T + 273.15)) /masa_molar_h).sqrt();
    let r2_he = ((gamma_he * R * (T + 273.15)) /masa_molar_he).sqrt();
    let r2_1 = r2_he/r2_h;
    let r2_2 = r2_h/r2_he;
    println!("para el hidrogeno: {}\n", r2_h);
    println!("para el helio: {}\n", r2_he);
    println!("la razon de helio con hidrogeno: {}", r2_1);
    println!("la razon de hidrogeno con helio: {}", r2_2);
    sl(Duration::from_secs(35));

    //tercer problema 
    println!("\n\n\n//tercer problema\n");
    println!("Determina la frecuencia fundamental y los primeros 3 sobretonos para un tubo de 20 cm a 20°C
    a) Si el tubo está abierto en ambos extremos
    b) Si el tubo está cerrado en uno de sus extremos
    Solución a) 858, 1720, 2570, 3430 Hz b) 429, 1290, 2140, y 3000 Hz\n");
    println!("weru weru weru, tra droko inirerey? por donde empezamos?.
    para hacer esto habria que pensarlo de la siguiente forma:
    para el tubo abierto son los numeros naturales como los valores de n
    para el tubo cerrado de los dos extremos solo los pares como los valores de n 
    y para el tubo cerrado de un extremo los valores nones como los valores de n\n");
    println!("las formulas son: 
    nv  nv 
    --  --
    2l  4l\n\n\n");
    T = 20.0f32;
    let mut n : f32 ;
    let mut v : f32 = 331.0 + (0.6 * T);
    let l : f32 = 0.2;
    let mut r3: f32;
    println!("los valores de las frecuencias en un tubo abierto");
    for i in 1..5 {
        n = i as f32;
        r3 = (n*v)/(2.0*l);
        println!("{}", r3);
    }

    let mut n_impar:Vec<f32> = Vec::new();
 
    n_impar.push(1.0);
    n_impar.push(3.0);
    n_impar.push(5.0);
    n_impar.push(7.0);

    println!("\nlos valores de las frecuencias en un tubo semi abierto");
    for i in n_impar {
        n = i;
        r3 = (n*v)/(4.0*l);
        println!("{}", r3);
    }
    sl(Duration::from_secs(50));

    // cuarto problema
    println!("\n\n\n//cuarto problema\n");
    println!("\n\n Calcule los niveles de intensidad en decibeles de sonidos de las siguientes intensidades:
    a) 1 x 10^-7 W/m^2
    b) 10^7 W/m^2
    c) 2 x 10^-3 W/m^2
    Solución a) 50 dB b) 130 dB c) 93 dB
    
                                         I
    La formula a utilizar es: β = 10 log -
                                         Io
    Io siendo 1*10^-12 W/m^2");
 
    let mut I: f32 = 1f32*10f32.powf(-7f32);
    let I0: f32 = 1f32*10f32.powf(-12f32);
    let mut byta = 10f32 * (I/I0).log10();

    println!("\n\t a){}dB", byta);
    I = 10f32.powf(7f32);
    byta = 10f32 * (I/I0).log10();
    println!("\t b){}dB", byta);
    I = 2f32*10f32.powf(-3f32);
    byta = 10f32 * (I/I0).log10();
    println!("\t b){}dB\n\n", byta);
    sl(Duration::from_secs(30));

    //problema 5
    println!("\n\n\n//quinto problema\n");
    println!("5. La frecuencia fundamental de un silbato de tren es 300 Hz, y la velocidad del tren es 
    20 m/s . En un día en que la temperatura es de 20°C, ¿Qué frecuencias escuchará el oyente
    que está inmóvil mientras el tren pasa haciendo sonar el silbato?
    Solución: 319, 300, y 283 Hz
    
    viendondo la recta de los valores de la velocidad(V)
    
    de el signo depende la direccion de la propagacion del sonido no la pocision
    respecto del ollente por lo que es una pocision relativa
    -                     0                       +
    |_____________________|_______________________|
    se acerca           neutro             se aleja
                          o
                         /|\\ yo
                         / \\
          coche -->
    
    como se acerca el coche seria un valor negativo de V
    
    La formula a utilizar es:;
       ___                 ___
       |      Vsonido        |
    f'=| ------------------  | X f
       | Vsonido +- Vfuente  |
       ---                 ---");
    T = 20.0f32;
    v = 331.0 + (0.6 * T);
    let fv = 20.0f32;
    let f = 300.0f32;
    let mut f_prima: f32;
    
    f_prima = (v/(v+fv))*f;
    println!("\n La velocidad del sonido es:331.0 + (0.6 * T)\nCuando el silvato esta a la altura de el ollente se escuchan 300hz");
    println!("Cuando se aleja {}hz",f_prima);
    f_prima = (v/(v-fv))*f;
    println!("Cuando se acerca {}hz", f_prima);
    sl(Duration::from_secs(60));

}